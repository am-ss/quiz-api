package com.alisson.quizzApi.Services;

import com.alisson.quizzApi.Models.AwnsersModel;
import com.alisson.quizzApi.Repository.AwnserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class ServicesTest {

    @Autowired
    AwnserService service;

    @Autowired
    AwnserRepository repository;

    @Test
    public void connectToRepository(){
        Long id = Long.valueOf(10);
        Boolean awnser = repository.existsById(id);
        Assert.assertTrue(awnser);
    }

    @Test
    public void connectToService(){
        Long id = Long.valueOf(10);
        AwnsersModel model = service.getAwnsersById(id);
        Assert.assertNotNull(model);
    }
}
