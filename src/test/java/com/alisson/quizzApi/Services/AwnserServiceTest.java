package com.alisson.quizzApi.Services;

import com.alisson.quizzApi.Models.AwnsersModel;
import com.alisson.quizzApi.Repository.AwnserRepository;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest
public class AwnserServiceTest {


    @Autowired
    AwnserService service;

    @Autowired
    AwnserRepository repository;

    private Long AwnserIntegrationId;

    @Test
    @DisplayName("Deve pegar lista de resposta com sucesso.")
    public void AgetSuccessAwnserList(){
       List<AwnsersModel> awnsers = service.getAllAwnsers();
       int lenght = awnsers.size();
        Assert.assertTrue(lenght > 0);
    }

    @Test
    @DisplayName("Deve pegar uma resposta com sucesso.")
    public void BgetSuccessAwnserById(){
        AwnsersModel awnsers = service.getAwnsersById(10L);
        Assert.assertNotNull(awnsers);
    }

    @Test
    @DisplayName("Deve cadastrar uma resposta com sucesso.")
    public void CregisterNewAwnserTest(){

        AwnsersModel awnser = new AwnsersModel("Resposta correta teste", true, 4);

        AwnsersModel awnsers = service.registerNewAwnser(awnser);
        this.AwnserIntegrationId = awnser.getId();
        Assert.assertNotNull(awnsers);
    }

    @Test
    @DisplayName("Deve atualizar uma resposta com sucesso.")
    public void DupdateNewAwnserTest(){

        AwnsersModel awnser = new AwnsersModel("Resposta correta teste - Atualizada", true, 4);
        AwnsersModel awnsers = service.updateAwnser(this.AwnserIntegrationId, awnser);
        Assert.assertNotNull(awnsers);
    }
}
