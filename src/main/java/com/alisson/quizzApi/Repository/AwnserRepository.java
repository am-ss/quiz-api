package com.alisson.quizzApi.Repository;

import com.alisson.quizzApi.Models.AwnsersModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AwnserRepository extends JpaRepository <AwnsersModel, Long> {

    @Query(value = "delete from awnser where question_id = :questionId", nativeQuery = true)
    public void deleteAwnserByQuestionId(@Param("questionId") Long questionId);

}
