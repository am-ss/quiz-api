package com.alisson.quizzApi.Repository;


import com.alisson.quizzApi.Models.LevelLabelModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LevelRepository extends JpaRepository<LevelLabelModel, Long> {

}
