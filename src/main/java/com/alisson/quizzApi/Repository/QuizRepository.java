package com.alisson.quizzApi.Repository;

import com.alisson.quizzApi.Models.QuizModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuizRepository extends JpaRepository <QuizModel, Long> {
}
