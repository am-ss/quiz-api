package com.alisson.quizzApi.Repository;

import com.alisson.quizzApi.Models.QuestionsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<QuestionsModel, Long> {

    @Query(value = "DELETE FROM question WHERE id = :id", nativeQuery = true)
    public void disableCheckAndDelete(@Param("id") Long id);

    @Query(value="SET foreign_key_checks=0;", nativeQuery = true)
    public void setSafeDelete();
}
