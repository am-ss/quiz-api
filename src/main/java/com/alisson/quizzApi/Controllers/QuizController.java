package com.alisson.quizzApi.Controllers;

import com.alisson.quizzApi.Models.QuizModel;
import com.alisson.quizzApi.Services.QuizService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="Quizzes")
@RequestMapping("/quiz")
public class QuizController {

    @Autowired
    private QuizService service;

    @GetMapping
    public List<QuizModel> getAllQuizzes(){
        return service.getAllQuiz();
    }

    @GetMapping("/{id}")
    public QuizModel getQuizById(@PathVariable("id") Long id){
        return  service.findQuizById(id);
    }

    @PostMapping
    public QuizModel registerNewQuiz(@RequestBody QuizModel quiz){
        return service.registerQuiz(quiz);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletQuiz(@PathVariable("id") Long id){
        return service.deleteQuiz(id);
    }
}
