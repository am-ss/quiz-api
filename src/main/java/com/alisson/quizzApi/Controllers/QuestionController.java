package com.alisson.quizzApi.Controllers;

import com.alisson.quizzApi.Models.QuestionsModel;
import com.alisson.quizzApi.Services.QuestionService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api
@RequestMapping("/questions")
public class QuestionController {

    @Autowired
    private QuestionService service;

    @GetMapping
    public List<QuestionsModel> getAllQuestions(){
        return service.getAllQuestions();
    }

    @GetMapping("/{id}")
    public QuestionsModel getQuestionById(@PathVariable("id") Long id){
        return service.getQuestionById(id);
    }

    @PostMapping
    public QuestionsModel registerNewQuestion (@RequestBody QuestionsModel question){
        return service.registerNewQuestion(question);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteQuestion(@PathVariable("id") Long id){
        return service.deleteQuestion(id);
    }
}
