package com.alisson.quizzApi.Controllers;

import com.alisson.quizzApi.Models.LevelLabelModel;
import com.alisson.quizzApi.Services.LevelLabelService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="LabelLevels")
@RequestMapping("/level")
public class LevelController {

    @Autowired
    private LevelLabelService service;

    @GetMapping
    public List<LevelLabelModel> findAllLevels(){
        return service.getAllLevels();
    }

    @GetMapping("/{id}")
    public LevelLabelModel findLevelById(@PathVariable("id") Long id){
        return service.getLevelById(id);
    }

    @PostMapping
    public LevelLabelModel registerNewLevel(@RequestBody LevelLabelModel level){
        return service.registerNewLevel(level);
    }
}
