package com.alisson.quizzApi.Controllers;

import com.alisson.quizzApi.Models.AwnsersModel;
import com.alisson.quizzApi.Models.Response;
import com.alisson.quizzApi.Services.AwnserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api("Respostas")
@RequestMapping("/awnsers")
public class AwnserController {

    @Autowired
    private AwnserService service;

    @GetMapping
    public List<AwnsersModel> getAllAwnsers(){
        return service.getAllAwnsers();
    }

    @PostMapping("/{id}")
    public AwnsersModel getAwnserById(@PathVariable("id") Long id){
        return service.getAwnsersById(id);
    }

    @PostMapping
    public AwnsersModel registerNewAwnser(@RequestBody AwnsersModel awnser){
        return service.registerNewAwnser(awnser);
    }

    @PutMapping("/{id}")
    public AwnsersModel updateAwnser(@PathVariable("id") Long id, @RequestBody AwnsersModel awnser){
        return service.updateAwnser(id, awnser);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteAwnser(@PathVariable("id") Long id){
        return service.deleteAwnser(id);
    }

}
