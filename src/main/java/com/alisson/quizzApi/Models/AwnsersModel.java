package com.alisson.quizzApi.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="awnser")
public class AwnsersModel implements Serializable {

    private static final long serialVersionUID = 1L;

    public AwnsersModel (String title, boolean isRight, int questionId){
        this.title = title;
        this.isRight = isRight;
        this.questionId = questionId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="title", length = 50, nullable = false)
    private String title;

    @Column(name="is_right", nullable = false)
    private boolean isRight;

    @Column(name="question_id", nullable = false)
    private int questionId;
}
