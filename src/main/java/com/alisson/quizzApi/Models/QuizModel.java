package com.alisson.quizzApi.Models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="quiz")
public class QuizModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="title", length = 50, nullable = false)
    private String title;

    @Column(name="quest_answered", length = 50, nullable = false)
    private int questAnswered;

    @Column(name="image", length = 500, nullable = false)
    private String imagePath;

    @Column(name="level_id", nullable = false)
    private int levelId;

    @OneToMany(targetEntity=QuestionsModel.class, mappedBy="quiz_id",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<QuestionsModel> questions;


}
