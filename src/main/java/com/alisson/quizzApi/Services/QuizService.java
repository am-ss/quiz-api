package com.alisson.quizzApi.Services;

import com.alisson.quizzApi.Models.QuizModel;
import com.alisson.quizzApi.Repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizService {

    @Autowired
    private QuizRepository repository;

    public List<QuizModel> getAllQuiz(){
        return repository.findAll();
    }

    public QuizModel findQuizById(Long id){
        return repository.findById(id).get();
    }

    public QuizModel registerQuiz(QuizModel quiz){
        return repository.save(quiz);
    }

    public ResponseEntity<String> deleteQuiz(Long id){
        try{
            QuizModel quiz = repository.findById(id).get();
            repository.delete(quiz);
            return new ResponseEntity<>("Deletado com sucesso.", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Erro ao deletar.", HttpStatus.BAD_REQUEST);
        }

    }
}
