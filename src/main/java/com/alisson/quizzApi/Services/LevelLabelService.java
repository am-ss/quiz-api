package com.alisson.quizzApi.Services;
import com.alisson.quizzApi.Models.LevelLabelModel;
import com.alisson.quizzApi.Repository.LevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class LevelLabelService {

    @Autowired
    LevelRepository repository;

    public List<LevelLabelModel> getAllLevels(){
        return repository.findAll();
    }

    public LevelLabelModel getLevelById(Long id){
        return repository.findById(id).get();
    }

    public LevelLabelModel registerNewLevel (LevelLabelModel level){
        return repository.save(level);
    }

}
