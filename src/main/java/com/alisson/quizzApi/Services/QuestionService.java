package com.alisson.quizzApi.Services;

import com.alisson.quizzApi.Models.AwnsersModel;
import com.alisson.quizzApi.Models.QuestionsModel;
import com.alisson.quizzApi.Repository.AwnserRepository;
import com.alisson.quizzApi.Repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class QuestionService {

    @Autowired
    private QuestionRepository repository;

    @Autowired
    private AwnserRepository awnserRepository;

    @Autowired
    private AwnserService awnserService;

    public List<QuestionsModel> getAllQuestions(){
        return repository.findAll();
    }

    public QuestionsModel getQuestionById(Long id){
        return repository.findById(id).get();
    }

    public QuestionsModel registerNewQuestion(QuestionsModel question){
        return repository.save(question);
    }

    public ResponseEntity<String> deleteQuestion(Long id){

        QuestionsModel question = repository.findById(id).get();

       try{
           repository.delete(question);
            return new ResponseEntity<>("Deletado com sucesso.", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Erro ao deletar.", HttpStatus.BAD_REQUEST);
        }

    }


}
