package com.alisson.quizzApi.Services;

import com.alisson.quizzApi.Models.AwnsersModel;
import com.alisson.quizzApi.Models.Response;
import com.alisson.quizzApi.Repository.AwnserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AwnserService {

    @Autowired
    private AwnserRepository repository;

    public List<AwnsersModel> getAllAwnsers (){
        return repository.findAll();
    }

    public AwnsersModel getAwnsersById (Long id){
        return repository.findById(id).get();
    }

    public AwnsersModel registerNewAwnser(AwnsersModel awnser){
        return repository.save(awnser);
    }

    public AwnsersModel updateAwnser(Long id, AwnsersModel awnser){
        awnser.setId(id);
        return repository.save(awnser);
    }

    public ResponseEntity<Response> deleteAwnser(Long id){
        AwnsersModel awnser = repository.findById(id).get();
        try{
            repository.delete(awnser);
            return new ResponseEntity<>(new Response(200, "Deletado com sucesso."), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new Response(400, "Erro ao deletar."), HttpStatus.BAD_REQUEST);
        }

    }

}
